package loadBalancer

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"reverseProxy/pkg/constant"
	"reverseProxy/pkg/util"
	"time"
)

type roundRobinEntryData struct {
	Index  int
	IsDown []bool
}

func SendRequest(client *http.Client, req *http.Request, destination util.Destination, requestURI string) (*http.Response, error) {
	switch destination.LoadBalancerMethod {
	case constant.ROUND_ROBIN:
		return sendRoundRobinRequest(client, req, destination, requestURI)

	case constant.NONE:
		return sendRequest(client, req, destination.Addresses[0], requestURI)

	default:
		return nil, errors.New("invalid load balancing algorithm")
	}
}

func markAddressAsNotAvailable(destination util.Destination, addressIndex int) {
	var identifier = destination.Identifier
	var isDown []bool = roundRobinEntries[identifier].IsDown

	isDown[addressIndex] = true
	roundRobinEntries[identifier] = roundRobinEntryData{
		Index:  addressIndex,
		IsDown: isDown,
	}

	var timer *time.Timer = time.NewTimer(10 * time.Second)

	go func(localAddressIndex int, localIdentifier string) {
		<-timer.C
		isDown[localAddressIndex] = false
		roundRobinEntries[localIdentifier] = roundRobinEntryData{
			Index:  localAddressIndex,
			IsDown: isDown,
		}
	}(addressIndex, identifier)
}

func sendRequest(client *http.Client, req *http.Request, serverAddress string, requestURI string) (*http.Response, error) {
	var err error

	respBody, err := io.ReadAll(req.Body)

	if err != nil {
		return nil, err
	}

	redirectReq, err := http.NewRequest(req.Method, fmt.Sprintf("%s%s", serverAddress, requestURI), bytes.NewReader(respBody))

	if err != nil {
		return nil, err
	}

	redirectReq.Host = req.Host
	for key, value := range req.Header {
		for _, headerItem := range value {
			redirectReq.Header.Set(key, headerItem)
		}
	}

	resp, err := client.Do(redirectReq)

	if err != nil {
		return nil, err
	}

	return resp, nil
}
