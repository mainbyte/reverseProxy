package loadBalancer

import (
	"errors"
	"net/http"
	"reverseProxy/pkg/util"
)

var roundRobinEntries map[string]roundRobinEntryData

func init() {
	roundRobinEntries = make(map[string]roundRobinEntryData)
}

func sendRoundRobinRequest(client *http.Client, req *http.Request, destination util.Destination, requestURI string) (*http.Response, error) {
	var err error
	var response *http.Response
	var requestIndex int

	_, exists := roundRobinEntries[destination.Identifier]

	if !exists {
		roundRobinEntries[destination.Identifier] = roundRobinEntryData{
			Index:  0,
			IsDown: make([]bool, len(destination.Addresses)),
		}
	}

	for i := 0; i < len(destination.Addresses); i++ {
		var addressIndex = roundRobinEntries[destination.Identifier].Index

		if roundRobinEntries[destination.Identifier].IsDown[addressIndex] == true {
			incrementIndex(destination, addressIndex)
			continue
		}

		requestIndex = addressIndex
		response, err = sendRequest(client, req, destination.Addresses[addressIndex], requestURI)

		incrementIndex(destination, addressIndex)

		if err != nil {
			markAddressAsNotAvailable(destination, requestIndex)

		} else {
			return response, nil
		}
	}

	return nil, errors.New("failed to access page")
}

func incrementIndex(destination util.Destination, addressIndex int) {
	roundRobinEntry := roundRobinEntries[destination.Identifier]
	roundRobinEntry = roundRobinEntryData{
		Index:  addressIndex + 1,
		IsDown: roundRobinEntries[destination.Identifier].IsDown,
	}

	if roundRobinEntry.Index >= len(destination.Addresses) {
		roundRobinEntry.Index = 0
	}

	roundRobinEntries[destination.Identifier] = roundRobinEntry
}
