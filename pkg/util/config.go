package util

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	HttpPort     int           `json:"httpPort"`
	HttpsPort    int           `json:"httpsPort"`
	AllowHTTP    bool          `json:"allowHTTP"`
	Destinations []Destination `json:"destinations"`
}

type Destination struct {
	Identifier         string   `json:"identifier"`
	Type               string   `json:"type"`
	LoadBalancerMethod string   `json:"loadBalancerMethod"`
	Addresses          []string `json:"addresses"`
}

func LoadConfig(configFilePath string) Config {
	var err error
	var configBytes []byte
	var config Config

	if err != nil {
		log.Panic(err)
	}

	configBytes, err = os.ReadFile(configFilePath)

	if err != nil {
		log.Panic(err)
	}

	err = json.Unmarshal(configBytes, &config)

	if err != nil {
		log.Panic(err)
	}

	return config
}
