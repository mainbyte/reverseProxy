package logger

import (
	"fmt"
	"os"
	"time"
)

var logFilePath string = "log.log"
var file *os.File
var serviceName string

func init() {
	var err error

	file, err = os.OpenFile(logFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		panic(err)
	}
}

func Info(sessionKey string, message string) {
	write(sessionKey+" "+message, "INFO")
}

func Error(sessionKey string, err error) {
	if err == nil {
		return
	}

	write(sessionKey+" error: "+err.Error(), "ERROR")
}

func Panic(sessionKey string, err error) {
	write(sessionKey+" "+err.Error(), "PANIC")
	panic(err)
}

func write(message string, logType string) {
	var err error
	serviceName = os.Getenv("SERVICE_NAME")

	message = fmt.Sprintf("%s %s %s %s", serviceName, getTimestamp(), logType, message)
	fmt.Println(message)

	_, err = file.Write([]byte(message + "\n"))

	if err != nil {
		fmt.Println("Failed to write to the log file! Error: " + err.Error())
	}
}

func getTimestamp() string {
	return time.Now().Format(time.RFC3339Nano)
}
