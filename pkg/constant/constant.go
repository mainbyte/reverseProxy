package constant

const (
	NONE                 = "NONE"
	ROUND_ROBIN          = "ROUND_ROBIN"
	WEIGHTED_ROUND_ROBIN = "WEIGHTED_ROUND_ROBIN"
	RESOURCE_BASE_METHOD = "RESOURCE_BASE_METHOD"

	LOG_FILE_PATH = "reverseProxy.log"
)
