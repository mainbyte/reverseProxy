# ReverseProxy

## Description
ReverseProxy allows the user to connect multiple computers to the network using only 2 ports. You can have any number of services hosted on servers, virtual machines or any other mediums and make them available outside their network by using only 2 ports (one for HTTP and one for HTTPS). The application is fast, easy to use and can be used by both professionals and beginners.

## Installation
Clone the repo, `cd` inside the project's directory and depending on your CPU architecture, run the appropriate `make` command.
For ARM64 CPUs, run the command `make buildARM64` and for X64 CPUs, run `make build`.
These commands will create binaries in the `bin` directory.

## Usage
Take the binary from the `bin` directory and run it with these parameters:
`./reverseProxy -certs <path_to_the_directory_containing_the_https_certificates> -config <path_to_the_config_file> -log <path_to_the_log_file>`

ReverseProxy is able to serve HTTPS content and thus requires HTTP certificates. The certificates have to be placed in the same directory and the public key must have the name `fullchain.pem` while the private key must be names `privkey.pem`.

The config file is a json file that contains the description of the destinations and how to reach them:

```JSON
{
	"httpPort": 8080,
	"httpsPort": 4430,
	"allowHTTP": true,
	"destinations": [
		{
			"identifier": "myApp0",
			"type": "subdomain",
			"loadBalancerMethod": "NONE",
			"addresses": [ "http://192.168.1.10:8080" ]
		},
		{
			"identifier": "myApp1",
			"type": "subdomain",
			"loadBalancerMethod": "ROUND_ROBIN",
			"addresses": [ "http://192.168.1.20:8080", "http://192.168.1.21:8080" ]
		},
		{
			"identifier": "api",
			"type": "path",
			"loadBalancerMethod": "NONE",
			"addresses": [ "http://192.168.1.30:8080"]
		}
	]
}
```

The log file contains data about the traffic and is created at the start of the application.

## Support
In order to get help, please create an issue here: https://gitlab.com/mainbyte/reverseProxy/-/issues.

## Contributing
Right now, PRs are not accepted but you are welcomed to fork the project and use it in the way you want.

## License
To be added.

## Project status
Currently maintained.