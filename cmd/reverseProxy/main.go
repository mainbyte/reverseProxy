package main

import (
	"flag"
	"os"
	"reverseProxy/internal/reverseProxy/transport"
	"reverseProxy/pkg/util"
)

func main() {
	var config util.Config
	var configFilePath *string
	var certificatesDirPath *string
	var logFilePath *string

	configFilePath = flag.String("config", "config.json", "set config file path")
	certificatesDirPath = flag.String("certs", "certificates", "set HTTPS certificates path")
	logFilePath = flag.String("log", "log.log", "set log file path")

	flag.Parse()

	os.Setenv("SERVICE_NAME", "reverseProxy")
	os.Setenv("CERTIFICATES_DIR_PATH", *certificatesDirPath)
	os.Setenv("LOG_FILE_PATH", *logFilePath)

	config = util.LoadConfig(*configFilePath)

	transport.MountRoutes(config)
}
