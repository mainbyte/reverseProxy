all: build

build: buildAMD64 buildARM64

buildAMD64:
	GOARCH=amd64 go build -o bin/amd64/reverseProxy cmd/reverseProxy/main.go

buildARM64:
	GOARCH=arm64 go build -o bin/arm64/reverseProxy cmd/reverseProxy/main.go