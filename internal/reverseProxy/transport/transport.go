package transport

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"path"
	"reverseProxy/internal/reverseProxy/internal/domain"
	"reverseProxy/pkg/logger"
	"reverseProxy/pkg/util"
	"time"
)

func MountRoutes(config util.Config) {
	var err error
	var client *http.Client
	var transport http.RoundTripper
	var certificatesDirPath string = os.Getenv("CERTIFICATES_DIR_PATH")

	logger.Info("root", certificatesDirPath)

	transport = &http.Transport{
		MaxIdleConns:    10,
		IdleConnTimeout: 30 * time.Second,
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client = &http.Client{Transport: transport}

	logger.Info("root", fmt.Sprintf("Subdomain reverse proxy started on ports (HTTP): %d and (HTTPS): %d!", config.HttpPort, config.HttpsPort))

	domain.SetupDomain(config, client)

	if !config.AllowHTTP {
		go http.ListenAndServe(fmt.Sprintf(":%d", config.HttpPort), http.HandlerFunc(domain.RedirectToHTTPS))
	} else {
		go http.ListenAndServe(fmt.Sprintf(":%d", config.HttpPort), nil)
	}

	http.HandleFunc("/", domain.RedirectHandler)

	err = http.ListenAndServeTLS(fmt.Sprintf(":%d", config.HttpsPort), path.Join(certificatesDirPath, "/fullchain.pem"), path.Join(certificatesDirPath, "/privkey.pem"), nil)

	if err != nil {
		logger.Panic("root", err)
	}
}
