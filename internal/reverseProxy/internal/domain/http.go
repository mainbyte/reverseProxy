package domain

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"

	"reverseProxy/pkg/constant"
	"reverseProxy/pkg/logger"
	"reverseProxy/pkg/util"
	"reverseProxy/pkg/util/loadBalancer"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

var config util.Config
var client *http.Client
var upgrader websocket.Upgrader

func RedirectHandler(writer http.ResponseWriter, req *http.Request) {
	var err error
	var destination *util.Destination
	var sessionKey string = uuid.New().String()

	var isWebSocket bool = false

	logger.Info(sessionKey, fmt.Sprintf("handling request with url %s", req.URL))

	destination, err = getDestination(req)

	if err != nil {
		logger.Error(sessionKey, err)
		writer.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(writer, constant.NotFoundResponse)
		return
	}

	if destination == nil || len(destination.Addresses) == 0 {
		logger.Error(sessionKey, errors.New("no destination found"))
		writer.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(writer, constant.NotFoundResponse)
		return
	}

	for _, header := range req.Header["Upgrade"] {
		if header == "websocket" {
			isWebSocket = true
			break
		}
	}

	if isWebSocket {
		RedirectToWebSocket(writer, req, sessionKey)
		return
	}

	var requestURI string = req.RequestURI

	if destination.Type == "path" {
		requestURI = "/" + strings.SplitN(requestURI, "/", 3)[2]
	}

	logger.Info(sessionKey, fmt.Sprintf("addresses: %v, request URI: %s", destination.Addresses, requestURI))

	resp, err := loadBalancer.SendRequest(client, req, *destination, requestURI)

	if err != nil {
		logger.Error(sessionKey, err)
		fmt.Fprintf(writer, `{ "error": "%s" }`, err.Error())

		return
	}

	for key, value := range resp.Header {
		for _, headerItem := range value {
			writer.Header().Set(key, headerItem)
		}
	}

	respBody, err := io.ReadAll(resp.Body)

	if err != nil {
		fmt.Println(err)
		return
	}

	defer resp.Body.Close()

	writer.WriteHeader(resp.StatusCode)
	io.Copy(writer, strings.NewReader(string(respBody)))
}

func RedirectToHTTPS(writer http.ResponseWriter, req *http.Request) {
	var targetAddress string = "https://" + req.Host + req.URL.Path

	if len(req.URL.RawQuery) > 0 {
		targetAddress += "?" + req.URL.RawQuery
	}

	logger.Info("root", "redirect to: %s"+targetAddress)

	http.Redirect(writer, req, targetAddress, http.StatusFound)
}

func SetupDomain(newConfig util.Config, newClient *http.Client) {
	config = newConfig
	client = newClient

	upgrader = websocket.Upgrader{
		ReadBufferSize:  4096,
		WriteBufferSize: 4096,
		CheckOrigin: func(req *http.Request) bool {
			return true
		},
	}
}

func getDestination(req *http.Request) (*util.Destination, error) {
	var requestDestination *util.Destination
	var subdomain string = strings.Split(req.Host, ".")[0]
	var pathTokens []string = strings.Split(req.URL.Path, "/")
	var path string

	if len(pathTokens) > 1 {
		path = pathTokens[1]
	}

	for _, destination := range config.Destinations {
		if !isValidType(destination.Type) {
			continue
		}
		if destination.Type == "subdomain" {
			if destination.Identifier == subdomain {
				requestDestination = &destination
				break
			}
		} else if destination.Type == "path" {
			if destination.Identifier == path {
				requestDestination = &destination
				break
			}
		}
	}

	if requestDestination == nil {
		return nil, fmt.Errorf("no destination found! full URL: %s %s%s, subdomain: %s, path: %s", req.Proto, req.Host, req.URL, subdomain, path)
	}

	return requestDestination, nil
}

func isValidType(destinationType string) bool {
	var validTypes []string = []string{"subdomain", "path"}

	for _, validType := range validTypes {
		if destinationType == validType {
			return true
		}
	}
	return false
}
