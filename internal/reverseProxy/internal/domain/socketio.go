package domain

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"reverseProxy/pkg/logger"
	"reverseProxy/pkg/util"
	"strings"
	"sync"

	"github.com/gorilla/websocket"
)

func RedirectToWebSocket(writer http.ResponseWriter, req *http.Request, sessionKey string) {
	var err error
	var destination *util.Destination

	destination, err = getDestination(req)

	if err != nil {
		logger.Error(sessionKey, err)
		return
	}

	if destination == nil {
		logger.Error(sessionKey, errors.New("destination is empty! request URL: "+req.URL.String()))
		return
	}

	var destinationAddress = destination.Addresses[0] // currently, does not support load balancing
	destinationAddress = strings.Split(destinationAddress, "://")[1]
	destinationAddress = "ws://" + destinationAddress + req.RequestURI

	respBody, err := io.ReadAll(req.Body)

	if err != nil {
		logger.Error(sessionKey, err)
		return
	}

	redirectReq, err := http.NewRequest(req.Method, fmt.Sprintf("%s%s", destinationAddress, req.RequestURI), bytes.NewReader(respBody))

	if err != nil {
		logger.Error(sessionKey, err)
		return
	}

	if len(destination.Addresses) == 0 {
		writer.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(writer, "no destination found")
		return
	}

	redirectReq.Host = req.Host
	for key, value := range req.Header {
		for _, headerItem := range value {
			redirectReq.Header.Set(key, headerItem)
		}
	}

	conn, err := upgrader.Upgrade(writer, redirectReq, nil)

	if err != nil {
		logger.Error(sessionKey, err)
		return
	}
	defer conn.Close()

	destConn, _, err := websocket.DefaultDialer.Dial(destinationAddress, nil)

	if err != nil {
		logger.Error(sessionKey, err)
		return
	}
	defer destConn.Close()

	var waitGroup sync.WaitGroup
	waitGroup.Add(2)

	// reading goroutine
	go func(wg *sync.WaitGroup) {
		for {
			// read from caller
			messageType, message, err := conn.ReadMessage()
			fmt.Println(string(message))
			if err != nil {
				logger.Error(sessionKey, err)
				break
			}

			// write to destination
			err = destConn.WriteMessage(messageType, message)

			if err != nil {
				logger.Error(sessionKey, err)
				break
			}
		}
		wg.Done()
	}(&waitGroup)

	// writing goroutine
	go func(wg *sync.WaitGroup) {
		for {
			// read response from destination
			messageType, destResponse, err := destConn.ReadMessage()

			if err != nil {
				logger.Error(sessionKey, err)
				break
			}

			// send response to caller
			err = conn.WriteMessage(messageType, destResponse)

			if err != nil {
				logger.Error(sessionKey, err)
				break
			}
		}
		wg.Done()
	}(&waitGroup)

	waitGroup.Wait()
}
